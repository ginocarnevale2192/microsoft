package utils;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.json.Json;

import java.io.FileReader;

public class JsonParser {
    private final JSONParser jsonParser;
    private JSONObject obj;

    public JsonParser() {
        jsonParser = new JSONParser();
        obj = new JSONObject();
    }

    public String getSearchTerm() {
        try {
            FileReader fileReader = new FileReader("src/test/data/data.json");
            Object file = jsonParser.parse(fileReader);
            obj = (JSONObject) file;

        } catch (Exception e) {
            return "error";
        }
        return obj.get("searchTerm").toString();
    }
}
