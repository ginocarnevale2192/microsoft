package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class homePage extends microsoftPageModel {


    @FindBy(id = "shellmenu_2")
    private WebElement windowsLBL;

    public homePage(WebDriver webdriver) {
        super("", webdriver);
    }

    public void clickOn_windowsLabel(){
        windowsLBL.click();
    }

}
