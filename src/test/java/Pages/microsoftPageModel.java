package Pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

public abstract class microsoftPageModel {
    String rootURL;
    String pageUrl ;
    WebDriver driver ;

    public microsoftPageModel(String pageUrl, WebDriver driver) {
        this.rootURL = "https://www.microsoft.com/es-mx/";
        this.pageUrl = pageUrl;
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    public String getURL(){
        return pageUrl.equals("") ? rootURL : pageUrl;
    }

    public WebDriver getDriver(){
        return driver;
    }

}
