package Pages.Sections.Windows;

import Pages.microsoftPageModel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class windowsPage extends microsoftPageModel {


    @FindBy(xpath = "//*[@id='search']")
    private WebElement searchBTN;

    @FindBy(xpath = "//*[@id='cli_shellHeaderSearchInput']")
    private WebElement searchTB;


    public windowsPage(WebDriver webdriver) {
        super("/windows/?r=1", webdriver);
    }

    public void clickOn_searchBox() {
        searchBTN.click();
    }

    public void sendKeysTo_searchTextBox(String query) {
        searchTB.sendKeys(query);
    }


}
