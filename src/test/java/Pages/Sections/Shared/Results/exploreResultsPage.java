package Pages.Sections.Shared.Results;

import Pages.microsoftPageModel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

// https://www.microsoft.com/es-mx/search/explore?q=Xbox
public class exploreResultsPage extends microsoftPageModel {


    @FindBy(xpath = "//header/a[1]")
    private WebElement comprarTAB;

    @FindBy(xpath = "//*[@id='R1MarketRedirect-close']")
    private WebElement languagePU;

    public exploreResultsPage(WebDriver webdriver) {
        super("/search/explore", webdriver);
    }

    public void clickOn_comprarTab() {
        comprarTAB.click();
    }

    public boolean visibility_languagePopup(){
        return languagePU.isDisplayed();
    }

    public void close_LanguagePopup(){
        if(languagePU.isDisplayed()){
            languagePU.click();
        }
    }
}
