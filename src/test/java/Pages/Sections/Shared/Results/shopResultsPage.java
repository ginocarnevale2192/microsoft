package Pages.Sections.Shared.Results;

import Pages.microsoftPageModel;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

// https://www.microsoft.com/es-mx/search/shop?q=Xbox
public class shopResultsPage extends microsoftPageModel {


    @FindBy(xpath = "//span[text()='Aplicaciones']")
    private WebElement appTAB;

    @FindBy(css = "div.m-channel-placement-item")
    private List<WebElement> appITEMS;

    @FindBy(xpath = "//span[text()='Siguiente']")
    private WebElement nextPageBTN;

    @FindBy(xpath = "//span[text()='1']")
    public WebElement firstPageLNK;

    @FindBy(xpath = "//a//div//div//span[contains(text(),'$')]")
    private WebElement firstPricedITEM;

    public shopResultsPage(WebDriver webdriver) {
        super("/search/shop", webdriver);
    }

    public void clickOn_appTab() {
        appTAB.click();
    }

    public int count_appItems() {
        return appITEMS.size();
    }

    public String getText_appItems() {
        StringBuilder titles = new StringBuilder();
        for (int i = 0; i < appITEMS.size(); i++) {
            titles.append("Title Number ").append(i).append(": ").append(appITEMS.get(i).getText()).append("\n");
        }
        return titles.toString();
    }

    public void clickOn_nextPageButton() {
        nextPageBTN.click();
    }

    public void clickOn_firstPage() {
        firstPageLNK.click();
    }

    public void clickOn_firstPricedITEM() {
        firstPricedITEM.click();
    }

    public String getText_firstPricedITEM() {
        return firstPricedITEM.getText();
    }

}
