package Pages.Sections.Shared.Applications;

import Pages.microsoftPageModel;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

// https://www.microsoft.com/es-mx/search/shop?q=Xbox
public class appPage extends microsoftPageModel {


    @FindBy(xpath = "//div[@role='button']")
    public WebElement registerPU;

    @FindBy(xpath = "//button[@id='ButtonPanel_buttonPanel_OverflowMenuTrigger']")
    public WebElement moreBTN;

    @FindBy(id = "buttonPanel_addToCartButton")
    public WebElement addToCartBTN;

    @FindBy(xpath = "//div//div//div//div//div//div//div//div//div//div//span[contains(text(),'$')]")
    public WebElement priceLBL;

    public appPage(WebDriver webdriver) {
        super("/search/shop", webdriver);
    }

    public void close_registerPU(){
        if(registerPU.isDisplayed()){
            registerPU.click();
        }
    }

    public String getText_priceLabel() {
        return priceLBL.getText();
    }

    public void clickOn_moreButton(){
        moreBTN.click();
    }

    public void clickOn_addToCartButton(){
        addToCartBTN.click();
    }

    public boolean isPresent_registerPopUp() {
        try {
            registerPU.isDisplayed();
            return true;
        } catch (NoSuchElementException ignored) {
            return false;
        }
    }

}
