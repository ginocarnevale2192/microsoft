package Pages.Sections.Shared.Cart;

import Pages.microsoftPageModel;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

//https://www.microsoft.com/es-mx/store/cart
public class cartItemsPage extends microsoftPageModel {


    @FindBy(xpath = "//a[contains(text(),'PUBG')]")
    public WebElement pubgITEM;

    @FindBy(xpath = "//p[contains(text(),'Tu carro está vacío.')]")
    public WebElement emptyCartLBL;

    @FindBy(xpath = "//button[contains(text(),'Quitar')]")
    public WebElement quitarBTN;

    public cartItemsPage(WebDriver webdriver) {
        super("store/cart", webdriver);
    }

    public void clickOn_windowsLabel() {
        pubgITEM.click();
    }

    public boolean isPresent_PubgItem() {
        try {
            pubgITEM.isDisplayed();
            return true;
        } catch (NoSuchElementException ignored) {
            return false;
        }
    }

    public void clickOn_quitarButton(){
        quitarBTN.click();
    }

    public boolean isCartEmpty() {
        try {
            emptyCartLBL.isDisplayed();
            return true;
        } catch (NoSuchElementException ignored) {
            return false;
        }
    }
}
